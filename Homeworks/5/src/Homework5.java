import java.util.Arrays;
import java.util.Calendar;
import java.util.Objects;

public class Homework5 {
    public static void main(String[] args) throws Exception {

        String[] santasHabits = {"demolish a house", "waving tail"};
        Pet santasLittleHelper = new Pet("dog", "Santa's Little Helper", 2015, 5, santasHabits);
        Pet princess = new Pet("pony", "Princess");

        Human homer = new Human("Homer", "Simpson", 1982 );
        Human marge = new Human("Marge", "Bouvier", 1983);
        Human lisa = new Human("Lisa", "Simpson", 2012);
        String[][] bartSchedule = new String[1][1];
        Human bart = new Human("Bart", "Simpson", 2010, 20, santasLittleHelper, bartSchedule);

        bart.greetPet();
        bart.describePet();
        bart.toString();
//        System.out.println(bart.equals(marge));

        Family family = new Family(homer, marge);
        family.addChild(bart);
        System.out.println(family.countFamily());
//        family.printAllChildren();
        family.addChild(lisa);
        System.out.println(family.countFamily());
//        family.printAllChildren();
        family.deleteChild(0);
        System.out.println(family.toString());
    }
}

class Pet {
    private String spicies;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    void eat() {
        System.out.println("Я кушаю!");
    }

    void respond() {
        System.out.println("Привет, хозяин. Я - " + this.nickname + ". Я соскучился!");
    }

    void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public String toString() {
        String allInfo = this.spicies + "={nickname='" + this.nickname + "', age=" + this.age + ", trickLevel=" + this.trickLevel + ", habits=" + Arrays.toString(this.habits) + "}";
        return allInfo;
    }

    public Pet(String spicies, String nickname) {
        this.spicies = spicies;
        this.nickname = nickname;
    }

    public Pet(String spicies, String nickname, int age, int trickLevel, String[] habits) throws Exception {
        this.spicies = spicies;
        this.nickname = nickname;
        this.age = age;

        this.habits = habits;

        if (trickLevel < 0 || trickLevel > 100) {
            throw new Exception("Trick level must be from 0 to 100");
        }
        this.trickLevel = trickLevel;
    }

    public Pet() {}

    public String getSpicies() {
        return spicies;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setSpicies(String spicies) {
        this.spicies = spicies;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        if (!super.equals(object)) return false;
        Pet pet = (Pet) object;
        return age == pet.age &&
                trickLevel == pet.trickLevel &&
                java.util.Objects.equals(spicies, pet.spicies) &&
                java.util.Objects.equals(nickname, pet.nickname) &&
                java.util.Arrays.equals(habits, pet.habits);
    }

    public int hashCode() {
        int result = Objects.hash(super.hashCode(), spicies, nickname, age, trickLevel);
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }
}

class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Family family;
    private String[][] schedule;

    void greetPet(){
        System.out.println("Привет, " + this.pet.getNickname());
    }
    void describePet() {

        Calendar calendar = Calendar.getInstance();
        int age = calendar.get(Calendar.YEAR) - this.year;

        String tricky;
        if (this.pet.getTrickLevel() > 50) {
            tricky = "очень хитрый";
        } else {
            tricky = "почти не хитрый";
        }
        System.out.println(String.format("У меня есть %s, ему %d лет, он %s.", this.pet.getSpicies(), age, tricky));
    }

    @Override
    public String toString() {
        String allInfo = "Human{name='" + this.name + "', surname='" + this.surname + "', year=" + this.year + ", iq=" + this.iq + ", schedule=" + this.schedule + "}";
        return allInfo;
//        return super.toString();
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, Pet pet, String[][] schedule) throws Exception {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.pet = pet;

        this.schedule = schedule;
        if(iq < 0 || iq > 100) {
            throw new Exception("IQ must be from 0 to 100");
        } else {
            this.iq = iq;
        }
    }

    public Human() {}

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getIq() {
        return iq;
    }

    public Pet getPet() {
        return pet;
    }

    public Family getFamily() {
        return family;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        if (!super.equals(object)) return false;
        Human human = (Human) object;
        return year == human.year &&
                iq == human.iq &&
                java.util.Objects.equals(name, human.name) &&
                java.util.Objects.equals(surname, human.surname) &&
                java.util.Objects.equals(pet, human.pet) &&
                java.util.Objects.equals(family, human.family) &&
                java.util.Arrays.equals(schedule, human.schedule);
    }

    public int hashCode() {
        int result = Objects.hash(super.hashCode(), name, surname, year, iq, pet, family);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }
}

class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;


    public Family(Human father, Human mother) {
        this.father = father;
        this.mother = mother;
        father.setFamily(this);
        mother.setFamily(this);
    }


    void addChild(Human human) {
        if(this.children == null) {
            this.children = new Human[1];
            this.children[0] = human;
        } else {
            Human[] tempArr = new Human[this.children.length + 1];
            for(int i = 0; i < this.children.length; i++) {
                tempArr[i] = this.children[i];
            }
            tempArr[this.children.length] = human;
            this.children = new Human[tempArr.length];
            for(int i = 0; i < tempArr.length; i++) {
                this.children[i] = tempArr[i];
            }
        }
    }

    void deleteChild(int delNumber) {
        Human[] tempArr = new Human[this.children.length-1];
        for(int i = 0; i < this.children.length; i++) {
            if(i < delNumber) {
                tempArr[i] = this.children[i];
            } else if (i > delNumber) {
                tempArr[i-1] = this.children[i];
            }
        }

        this.children = new Human[tempArr.length];
        for(int i = 0; i < tempArr.length; i++) {
            this.children[i] = tempArr[i];
        }
    };

    int countFamily() {
        return 2 + this.children.length;
    }

    public Human[] getChildren() {
        return children;
    }

//    public void printAllChildren() {
//        for(int i = 0; i < this.children.length; i++) {
//            System.out.println(this.children[i]);
//        }
//    }


    @Override
    public String toString() {
        String allInfo = "Family{father='" + this.father + "', mother='" + this.mother + "', children=" + this.children + ", pet=" + this.pet + "}";
        return allInfo;
    }

    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        if (!super.equals(object)) return false;
        Family family = (Family) object;
        return java.util.Objects.equals(mother, family.mother) &&
                java.util.Objects.equals(father, family.father) &&
                java.util.Arrays.equals(children, family.children) &&
                java.util.Objects.equals(pet, family.pet);
    }

    public int hashCode() {
        int result = Objects.hash(super.hashCode(), mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }
}