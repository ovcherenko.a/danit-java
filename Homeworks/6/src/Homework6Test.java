import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;

public class Homework6Test {
    private Homework6 homework;

    @Test
    public void toStringTest() {
        Human homer = new Human("Homer", "Simpson", 1982 );
        assertEquals(
                "Human{name='Homer', surname='Simpson', year=1982, iq=0, schedule=null}",
                homer.toString()
        );

        Pet princess = new Pet(Species.PONY, "Princess");
        assertEquals(
                "PONY={nickname='Princess', age=0, trickLevel=0, habits=null}",
                princess.toString()
        );
    }

    @Test
    public void deleteChildHumanTest() {
        Human homer = new Human("Homer", "Simpson", 1982 );
        Human marge = new Human("Marge", "Bouvier", 1983);
        Human bart = new Human("Bart", "Simpson", 2010);
        Human lisa = new Human("Lisa", "Simpson", 2012);
        Family family = new Family(homer, marge);
        family.addChild(bart);
        assertEquals(false, family.deleteChild(lisa));
        assertEquals(3, family.countFamily());
        family.addChild(lisa);
        assertEquals(true, family.deleteChild(bart));
        assertEquals(3, family.countFamily());
    }

    @Test
    public void deleteChildIntTest() {
        Human homer = new Human("Homer", "Simpson", 1982 );
        Human marge = new Human("Marge", "Bouvier", 1983);
        Human bart = new Human("Bart", "Simpson", 2010);
        Human lisa = new Human("Lisa", "Simpson", 2012);
        Family family = new Family(homer, marge);
        family.addChild(bart);
        family.addChild(lisa);
        assertEquals(true, family.deleteChild(1));
        assertEquals(false, family.deleteChild(-1));
        assertEquals(false, family.deleteChild(4));
        //family.deleteChild(1);

    }

    @Test
    public void addChildTest() {
        Human homer = new Human("Homer", "Simpson", 1982 );
        Human marge = new Human("Marge", "Bouvier", 1983);
        Human bart = new Human("Bart", "Simpson", 2010);
        Family family = new Family(homer, marge);
        family.addChild(bart);

        Human[] childrenList = family.getChildren();
        assertEquals(1, childrenList.length);
        assertEquals(bart, childrenList[0]);
    }

    @Test
    public void countFamilyTest() {
        Human homer = new Human("Homer", "Simpson", 1982 );
        Human marge = new Human("Marge", "Bouvier", 1983);
        Human bart = new Human("Bart", "Simpson", 2010);
        Family family = new Family(homer, marge);
        family.addChild(bart);

//        assertEquals(2, family.countFamily());
        assertEquals(3, family.countFamily());
    }

}
