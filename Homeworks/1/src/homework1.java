import java.util.*;

public class homework1 {
    public static void main(String[] args) {
        //User entering hit name
        System.out.println("/%Username%/, enter your name!");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();

        //Start message
        System.out.println("Let the game begin!");

        //Getting randome number
        Random random = new Random();
        int rndNumber = random.nextInt(101);
//        System.out.println(rndNumber);

        //User entering number
        String tempNum;
        do {
            System.out.println("Leather bag, what number have I just thought about?");
            tempNum = scanner.nextLine();
        } while (!tempNum.matches("\\d+"));
        int num = Integer.parseInt(tempNum);
        int nums[] = new int[1];
        nums[0] = num;

        //Hints for user
        while (num != rndNumber) {
            if(num < rndNumber) {
                System.out.println("Your number is too small. Please, try again.");
                num = Integer.parseInt(scanner.nextLine());
                nums = addToArray(nums, num);
            } else {
                System.out.println("Your number is too big. Please, try again.");
                num = Integer.parseInt(scanner.nextLine());
                nums = addToArray(nums, num);
            }
        }

        reverse(nums);

        //Congrats message
        System.out.println("Congratulations, " + name);
        System.out.println("Your numbers are: " + Arrays.toString(nums));
    }

    //Adding new int to array method
    public static int[] addToArray(int[] arr1, int num) {
        int tempNums[] = new int[arr1.length+1];
        for(int i = 0; i < arr1.length; i++) {
            tempNums[i] = arr1[i];
        }
        tempNums[tempNums.length-1] = num;

        arr1 = new int[tempNums.length];
        for(int i = 0; i < tempNums.length; i++) {
            arr1[i] = tempNums[i];
        }
        return arr1;
    }

    //Reverse array method
    public static void reverse(int[] input) {
        int n  = input.length;
        int temp = 0;

        for(int i=0; i < n; i++) {
            for(int j=1; j < (n-i); j++) {
                if(input[j-1] < input[j]){
                    temp = input[j-1];
                    input[j-1] = input[j];
                    input[j] = temp;
                }
            }
        }
    }
}
