import java.util.Arrays;
import java.util.Calendar;
import java.util.Objects;

public class Homework7 {
    public static void main(String[] args) throws Exception {

        String[] santasHabits = {"demolish a house", "waving tail"};
//        Pet santasLittleHelper = new Dog(Species.DOG, "Santa's Little Helper", 2015, 5, santasHabits);
//        Pet princess = new RoboCat(Species.PONY, "Princess");
        Pet santasLittleHelper = new Dog();
        Pet princess = new RoboCat();


        Human homer = new Human("Homer", "Simpson", 1982 );

        Human marge = new Human("Marge", "Bouvier", 1983);
        Human lisa = new Human("Lisa", "Simpson", 2012);

        // Заполнение расписания Барту
        String[][] bartSchedule = new String[2][2];
        bartSchedule[0][0] = DayOfWeek.MONDAY.name();
        bartSchedule[0][1] = Hobby.ACTOBATICS.name();
        bartSchedule[1][0] = DayOfWeek.TUESDAY.name();
        bartSchedule[1][1] = Hobby.ANIMATION.name();

        Human bart = new Human("Bart", "Simpson", 2010, 20, santasLittleHelper, bartSchedule);

        bart.greetPet();
        bart.describePet();
        bart.toString();
        System.out.println(bart.equals(marge));

        Family family = new Family(homer, marge);
        family.addChild(bart);
        System.out.println(family.countFamily());
//        family.printAllChildren();
        family.addChild(lisa);
        System.out.println(family.countFamily());
//        family.printAllChildren();

        System.out.println("DELETING CHILD");
        //System.out.println(family.deleteChild(0));
        System.out.println(family.deleteChild(bart));


//        family.deleteChild(0);
        System.out.println(family.toString());

        for(int i = 0; i < 1000000; i++) {
            new Human();
        }

    }
}

abstract class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    void eat() {
        System.out.println("Я кушаю!");
    }

    abstract void respond();

    @Override
    public String toString() {
    String allInfo = this.species + "={nickname='" + this.nickname + "', age=" + this.age + ", trickLevel=" + this.trickLevel + ", habits=" + Arrays.toString(this.habits) + "}";
    return allInfo;
    }

    public Pet(Species spicies, String nickname) {
        this.species = spicies;
        this.nickname = nickname;
    }

    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) throws Exception {
        this.species = species;
        this.nickname = nickname;
        this.age = age;

        this.habits = habits;

        if (trickLevel < 0 || trickLevel > 100) {
            throw new Exception("Trick level must be from 0 to 100");
        }
        this.trickLevel = trickLevel;
    }

    public Pet() {}

    public Species getSpecies() {
        return this.species;
    }

    public String getNickname() {
        return this.nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }

    public String[] getHabits() {
        return this.habits;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        if (!super.equals(object)) return false;
        Pet pet = (Pet) object;
        return age == pet.age &&
                trickLevel == pet.trickLevel &&
                java.util.Objects.equals(species, pet.species) &&
                java.util.Objects.equals(nickname, pet.nickname) &&
                java.util.Arrays.equals(habits, pet.habits);
    }

    public int hashCode() {
        int result = Objects.hash(super.hashCode(), species, nickname, age, trickLevel);
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }
    protected void finalize() {
        System.out.println("={nickname='" + this.nickname + "', age=" + this.age + ", trickLevel=" + this.trickLevel + ", habits=" + Arrays.toString(this.habits) + "}");
    }
}

class Fish extends Pet {
    void respond() {
        System.out.println("blub blub");
    }
}

class DomesticCat extends Pet {
    void respond() {
        System.out.println("Meow");
    }
}

class Dog extends Pet {
    void respond() {
        System.out.println("Woof");
    }

    public Dog() {
        super();
    }
}

class RoboCat extends Pet {
    void respond() {
        System.out.println("Beep beep");
    }
}

//class Pet {
//    private Species species;
//    private String nickname;
//    private int age;
//    private int trickLevel;
//    private String[] habits;
//
//
//    void respond() {
//        System.out.println("Привет, хозяин. Я - " + this.nickname + ". Я соскучился!");
//    }
//
//    void foul() {
//        System.out.println("Нужно хорошо замести следы...");
//    }
//
//    @Override
//    public String toString() {
//        String allInfo = this.species + "={nickname='" + this.nickname + "', age=" + this.age + ", trickLevel=" + this.trickLevel + ", habits=" + Arrays.toString(this.habits) + "}";
//        return allInfo;
//    }
//
//    public Pet(Species spicies, String nickname) {
//        this.species = spicies;
//        this.nickname = nickname;
//    }
//
//    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) throws Exception {
//        this.species = species;
//        this.nickname = nickname;
//        this.age = age;
//
//        this.habits = habits;
//
//        if (trickLevel < 0 || trickLevel > 100) {
//            throw new Exception("Trick level must be from 0 to 100");
//        }
//        this.trickLevel = trickLevel;
//    }
//
//    public Pet() {}
//
//    public Species getSpecies() {
//        return this.species;
//    }
//
//    public String getNickname() {
//        return this.nickname;
//    }
//
//    public int getAge() {
//        return age;
//    }
//
//    public int getTrickLevel() {
//        return this.trickLevel;
//    }
//
//    public String[] getHabits() {
//        return this.habits;
//    }
//
//    public void setSpecies(Species species) {
//        this.species = species;
//    }
//
//    public void setNickname(String nickname) {
//        this.nickname = nickname;
//    }
//
//    public void setAge(int age) {
//        this.age = age;
//    }
//
//    public void setTrickLevel(int trickLevel) {
//        this.trickLevel = trickLevel;
//    }
//
//    public void setHabits(String[] habits) {
//        this.habits = habits;
//    }
//
//    public boolean equals(Object object) {
//        if (this == object) return true;
//        if (object == null || getClass() != object.getClass()) return false;
//        if (!super.equals(object)) return false;
//        Pet pet = (Pet) object;
//        return age == pet.age &&
//                trickLevel == pet.trickLevel &&
//                java.util.Objects.equals(species, pet.species) &&
//                java.util.Objects.equals(nickname, pet.nickname) &&
//                java.util.Arrays.equals(habits, pet.habits);
//    }
//
//    public int hashCode() {
//        int result = Objects.hash(super.hashCode(), species, nickname, age, trickLevel);
//        result = 31 * result + Arrays.hashCode(habits);
//        return result;
//    }
//    protected void finalize() {
//        System.out.println("={nickname='" + this.nickname + "', age=" + this.age + ", trickLevel=" + this.trickLevel + ", habits=" + Arrays.toString(this.habits) + "}");
//    }
//}

class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Family family;
    private String[][] schedule;

    void greetPet(){
        System.out.println("Привет, " + this.pet.getNickname());
    }
    void describePet() {

        Calendar calendar = Calendar.getInstance();
        int age = calendar.get(Calendar.YEAR) - this.year;

        String tricky;
        if (this.pet.getTrickLevel() > 50) {
            tricky = "очень хитрый";
        } else {
            tricky = "почти не хитрый";
        }
        System.out.println(String.format("У меня есть %s, ему %d лет, он %s.", this.pet.getSpecies(), age, tricky));
    }

    @Override
    public String toString() {
        String allInfo = "Human{name='" + this.name + "', surname='" + this.surname + "', year=" + this.year + ", iq=" + this.iq + ", schedule=" + this.schedule + "}";
        return allInfo;
//        return super.toString();
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, Pet pet, String[][] schedule) throws Exception {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.pet = pet;
        this.schedule = schedule;
        if(iq < 0 || iq > 100) {
            throw new Exception("IQ must be from 0 to 100");
        } else {
            this.iq = iq;
        }
    }

    public Human() {}

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getIq() {
        return iq;
    }

    public Pet getPet() {
        return pet;
    }

    public Family getFamily() {
        return family;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        if (!super.equals(object)) return false;
        Human human = (Human) object;
        return year == human.year &&
                iq == human.iq &&
                java.util.Objects.equals(name, human.name) &&
                java.util.Objects.equals(surname, human.surname) &&
                java.util.Objects.equals(pet, human.pet) &&
                java.util.Objects.equals(family, human.family) &&
                java.util.Arrays.equals(schedule, human.schedule);
    }

    public int hashCode() {
        int result = Objects.hash(super.hashCode(), name, surname, year, iq, pet, family);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }

    protected void finalize() {
        System.out.println("Human{name='" + this.name + "', surname='" + this.surname + "', year=" + this.year + ", iq=" + this.iq + ", schedule=" + this.schedule + "}");
    }
}

class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;


    public Family(Human father, Human mother) {
        this.father = father;
        this.mother = mother;
        father.setFamily(this);
        mother.setFamily(this);
    }


    void addChild(Human human) {
        if(this.children == null) {
            this.children = new Human[1];
            this.children[0] = human;
        } else {
            Human[] tempArr = new Human[this.children.length + 1];
            for(int i = 0; i < this.children.length; i++) {
                tempArr[i] = this.children[i];
            }
            tempArr[this.children.length] = human;
            this.children = new Human[tempArr.length];
            for(int i = 0; i < tempArr.length; i++) {
                this.children[i] = tempArr[i];
            }
        }
    }

    boolean deleteChild(int delNumber) {
        if(delNumber > this.children.length-1 || delNumber < 0) {
            return false;
        } else {
            Human[] tempArr = new Human[this.children.length-1];
            for(int i = 0; i < this.children.length; i++) {
                if(i < delNumber) {
                    tempArr[i] = this.children[i];
                } else if (i > delNumber) {
                    tempArr[i-1] = this.children[i];
                }
            }

            this.children = new Human[tempArr.length];
            for(int i = 0; i < tempArr.length; i++) {
                this.children[i] = tempArr[i];
            }
            return true;
        }
    };

    boolean deleteChild(Human child) {
        boolean rtrn = false;

        for(int i = 0; i < this.children.length; i++){
            if(this.children[i].equals(child)){
                Human[] tempArr = new Human[this.children.length-1];
                for(int j = 0; j < this.children.length; j++) {
                    if (j < i) {
                        tempArr[j] = this.children[j];
                    } else if (j > i) {
                        tempArr[j - 1] = this.children[j];
                    }
                }
                this.children = new Human[tempArr.length];
                rtrn = true;
                break;
            } else {
                rtrn = false;
            }
        }
        return rtrn;
    }

    int countFamily() {
        if(this.children == null){
            return 2;
        } else {
            return 2 + this.children.length;
        }
    }

    public Human[] getChildren() {
        return children;
    }

//    public void printAllChildren() {
//        for(int i = 0; i < this.children.length; i++) {
//            System.out.println(this.children[i]);
//        }
//    }

    @Override
    public String toString() {
        String allInfo = "Family{father='" + this.father + "', mother='" + this.mother + "', children=" + this.children + ", pet=" + this.pet + "}";
        return allInfo;
    }

    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        if (!super.equals(object)) return false;
        Family family = (Family) object;
        return java.util.Objects.equals(mother, family.mother) &&
                java.util.Objects.equals(father, family.father) &&
                java.util.Arrays.equals(children, family.children) &&
                java.util.Objects.equals(pet, family.pet);
    }

    public int hashCode() {
        int result = Objects.hash(super.hashCode(), mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    protected void finalize() {
        System.out.println("Family{father='" + this.father + "', mother='" + this.mother + "', children=" + this.children + ", pet=" + this.pet + "}");
    }

}

enum Species {
    CAT,
    DOG,
    PONY,
    BADGER
}

enum Hobby {
    ACTOBATICS,
    ACTING,
    ANIMATION,
    AQUASCAPING,
    ASTROLOGY,
    ASTRONOMY
}

enum DayOfWeek {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
}