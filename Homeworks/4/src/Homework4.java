import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class Homework4 {
    public static void main(String[] args) throws Exception {
        String[] santasHabits = {"demolish a house", "waving tail"};
        Pet santasLittleHelper = new Pet("dog", "Santa's Little Helper", 2015, 5, santasHabits);
        Pet princess = new Pet("pony", "Princess");

        Human homer = new Human("Homer", "Simpson", 1982 );
        Human marge = new Human("Marge", "Bouvier", 1983);
        Human lisa = new Human("Lisa", "Simpson", 2012, marge, homer);
        String[][] bartSchedule = new String[1][1];
        Human bart = new Human("Bart", "Simpson", 2010, 20, santasLittleHelper, marge, homer, bartSchedule);

        bart.greetPet();
        bart.describePet();
        bart.toString();
    }
}

class Pet {
    String spicies;
    String nickname;
    int age;
    int trickLevel;
    String[] habits;

    void eat() {
        System.out.println("Я кушаю!");
    }

    void respond() {
        System.out.println("Привет, хозяин. Я - " + this.nickname + ". Я соскучился!");
    }

    void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public String toString() {
        String allInfo = this.spicies + "={nickname='" + this.nickname + "', age=" + this.age + ", trickLevel=" + this.trickLevel + ", habits=" + Arrays.toString(this.habits) + "}";
        return allInfo;
    }

    public Pet(String spicies, String nickname) {
        this.spicies = spicies;
        this.nickname = nickname;
    }

    public Pet(String spicies, String nickname, int age, int trickLevel, String[] habits) throws Exception {
        this.spicies = spicies;
        this.nickname = nickname;
        this.age = age;

        this.habits = habits;

        if (trickLevel < 0 || trickLevel > 100) {
            throw new Exception("Trick level must be from 0 to 100");
        }
        this.trickLevel = trickLevel;
    }

    public Pet() {}
}

class Human {
    String name;
    String surname;
    int year;
    int iq;
    Pet pet;
    Human mother;
    Human father;
    String[][] schedule;

    void greetPet(){
        System.out.println("Привет, " + this.pet.nickname);
    }
    void describePet() {

        //ДОДЕЛАТЬ
        Calendar calendar = Calendar.getInstance();

        int age = calendar.get(Calendar.YEAR) - this.year;

        String tricky;
        if (this.pet.trickLevel > 50) {
            tricky = "очень хитрый";
        } else {
            tricky = "почти не хитрый";
        }
        System.out.printf("У меня есть %s, ему %d лет, он %s.", this.pet.spicies, age, tricky);
    }

    @Override
    public String toString() {
        String allInfo = "Human{name='" + this.name + "', surname='" + this.surname + "', year=" + this.year + ", iq=" + this.iq +", mother=" + this.mother.name +", father=" + this.father.name + ", pet="+ this.pet.toString() + "}";
        return allInfo;
//        return super.toString();
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] schedule) throws Exception {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
        if(iq < 0 || iq > 100) {
            throw new Exception("IQ must be from 0 to 100");
        } else {
            this.iq = iq;
        }
    }

    public Human() {}
}