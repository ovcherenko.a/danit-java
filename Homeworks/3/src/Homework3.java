import java.util.Scanner;

public class Homework3 {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[][] scedule = new String[7][2];

        // Boring thing
        scedule[0][0] = "Sunday";
        scedule[0][1] = "do home work";
        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "buy one bottle of rum, one bottle of whiskey and pack of beer";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "drink rum";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "drink whiskey";
        scedule[5][0] = "Friday";
        scedule[5][1] = "drink beer";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "pray to our god Poseidon";

        //Asking in cycle
        Boolean isWorking = true;
        while(isWorking) {

            //Asking user day of the week
            System.out.println("Please, input day of the week");
            String dayOfWeek = scanner.nextLine();
            System.out.println(dayOfWeek);

            //Searching for answer
            switch(dayOfWeek.trim().toLowerCase()){
                case "monday":
                    System.out.println(scedule[1][1]);
                    break;
                case "tuesday":
                    System.out.println(scedule[2][1]);
                    break;
                case "wednesday":
                    System.out.println(scedule[3][1]);
                    break;
                case "thursday":
                    System.out.println(scedule[4][1]);
                    break;
                case "friday":
                    System.out.println(scedule[5][1]);
                    break;
                case "saturday":
                    System.out.println(scedule[6][1]);
                    break;
                case "sunday":
                    System.out.println(scedule[0][1]);
                    break;
                case "exit":
                    isWorking = false;
                    break;
                default:
                    System.out.println("Sorry, I don't understand you, please try again");
            }
        }
    }
}
