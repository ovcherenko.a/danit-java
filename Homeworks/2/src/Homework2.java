import java.util.Random;
import java.util.Scanner;

public class Homework2 {
    public static void main(String[] args) {

        //creating and filling table
        String[][] table;
        table = new String[5][5];
        for(int i = 0; i < table.length; i++) {
            for(int j = 0; j < table.length; j++) {
                table[i][j] = "-";
            }
        }


        Scanner scanner = new Scanner(System.in);


        Random random = new Random();
        int y = random.nextInt(5)+1;
        int x = random.nextInt(5)+1;

        int playerY = 0;
        int playerX = 0;
//        String regex = "[1-5]";

        System.out.println("All set. Get ready to rumble!");

        while(playerX != x || playerY != y) {
//            System.out.println(x + "x");
//            System.out.println(y + "y");
//            System.out.println(playerX + "playerX");
//            System.out.println(playerY  + "playerY");
            String playerYStr;
            String playerXStr;
            do {
                System.out.println("Enter a column value");
                playerYStr = scanner.nextLine();
                playerY = Integer.parseInt(playerYStr);
            } while (playerY < 0 && playerY > 6);

            do{
            System.out.println("Enter a string value");
            playerXStr = scanner.nextLine();
            playerX = Integer.parseInt(playerXStr);
            } while (playerX < 0  && playerX > 6);

            if (playerX == x && playerY == y) {
                table[playerX-1][playerY-1] = "x";
            } else {
                table[playerX-1][playerY-1] = "*";
            }
            drawTable(table);
            System.out.println("");
        };
        System.out.println("You have won!");
    }

    public static void drawTable(String[][] table) {

        for(int ii = 0; ii < table.length; ii++) {
            System.out.print(ii + " | ");
        }
        System.out.println(table.length + " |");
        for(int i = 0; i < table.length; i++) {

            System.out.print(i+1);
            System.out.print(" | ");
            for(int j = 0; j < table.length; j++) {
                System.out.print(table[i][j]);
                System.out.print(" | ");
            }
            System.out.println("");
        }

    }
}
